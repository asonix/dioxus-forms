{
  description = "form-testing";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      {
        packages = rec {
          form-testing = pkgs.callPackage ./form-testing.nix { };

          default = form-testing;
        };

        apps = rec {
          dev = flake-utils.lib.mkApp { drv = self.packages.${system}.pict-rs-proxy; };
          default = dev;
        };

        devShell = with pkgs; mkShell {
          nativeBuildInputs = [ cargo cargo-outdated cargo-zigbuild clippy gcc protobuf rust-analyzer rustc rustfmt taplo ];

          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
      });
}
