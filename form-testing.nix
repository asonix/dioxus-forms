{ lib
, makeWrapper
, nixosTests
, protobuf
, rustPlatform
, stdenv
}:

rustPlatform.buildRustPackage {
  pname = "form-testing";
  version = "0.1.0";
  src = ./.;
  cargoSha256 = "lv3ys8c2O9zCiJ6wnVrubGX8ylqh+oY7SK4eeFSjva4=";

  PROTOC = "${protobuf}/bin/protoc";
  PROTOC_INCLUDE = "${protobuf}/include";

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) form-testing; };

  meta = with lib; {
    description = "Test for dioxus form shenanigans";
    homepage = "https://git.asonix.dog/asonix/form-testing";
    license = with licenses; [ agpl3Plus ];
  };
}
